"""VPL - Atividade 1 (Somar inteiros)
Faça um programa que receba quatro números inteiros, calcule e mostre a soma desses números.
O usuário deverá digitar os valores para número1, número2, número3 e número4.
O programa deverá exibir o resultado da soma desses números inteiros digitados pelo usuário.
OBS: O programa não deverá colocar enfeites/perfumaria ao esperar o usuário digitar, bem como, ao mostrar o resultado na tela. Observe o exemplo a seguir.
Exemplo:
Dados de entrada:
5
2
9
7
Dados de saída:
23
"""

n1 = int(input(""))
n2 = int(input(""))
n3 = int(input(""))
n4 = int(input(""))

print(n1+n2+n3+n4)
