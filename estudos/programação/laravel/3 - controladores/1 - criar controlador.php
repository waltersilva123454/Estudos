<?php
/*
controladores ficam na pasta Projeto/APP/Http/controllers

para criar um controlador basta criar um arquivo php com o nome do controlador
dentro do arquivo devemos escrever uma classe php 
essa classe vai ser o controlador
a classe do controlador deve extender a classe Controller

ex:
*/
namespace App\http\contrroller;

class meuControlador extends Controller{
    //conteudo da classe
}
/*
podemos ainda criar controladores com o artisan.php

o comando para criar um controller com o artisan é 

make:controler

no terminal fica: 
php artisan make:controller meuControlador

o artisan fica na pasta principal do projeto

*/

